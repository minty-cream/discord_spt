Hello! I'm a bot to help you with your theater of mind game. I keep notes for you. Whenever I do any of these activities (except this one), I also show my notes afterwards, and pin them to the pinned messages board. I will only ever have one message pinned at a time.

!add_goal <name>:<description> #Add a goal necessary to complete the scene. Example: 

!add_goal Get honey:Pooh needs a jar of honey. He doesn't care how you get it, he just wants one.

!add_item <name>:<description> #Add an item relevant to the scene. Example !add_item Honey jar:A jar of honey. Pooh wanted one of these.

!add_person <name>:<description> #Add a person relevant to the scene. Example !add_person Pooh:A large intelligent bear. He wanted a jar of honey.

!add_location <name>:<description> #Add a location relevant to the scene. Example !add_location Pooh's house:A large hollowed tree stump that Pooh lives in.

!complete_goal <id> #When you complete a goal, be sure to run this to update the list.

!clear #Clear the goals, items, persons, and locations.

!help #Show this message!