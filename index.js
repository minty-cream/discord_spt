const Discord=require('discord.js')
const client=new Discord.Client();
const debug=require('debug');
const JSONPO = require('json-persistent-object');

let _store = new JSONPO('store.json');

if (!_store.locations){ _store.locations=[]; }
if (!_store.persons){ _store.persons=[]; }
if (!_store.items){ _store.items=[]; }
if (!_store.goals){ _store.goals=[]; }

client.on('ready',()=>{
	let _debug=debug('ready');
	_debug('Client Ready');
});

client.on('message',(message)=>{
	let _debug=debug('message');
	_debug('Message received. %o',message);

	//Commands
	// !add_goal <name>:<description>
	// !add_item <name>:<description>
	// !add_location <name>:<description>
	// !add_person <name>:<description>
	// !clear
	// !complete_goal <id>
	// !help
	// !show_notes

	if(message.content[0] === '!'){
		let _split=message.content.split(/ (.+)/);
		let _command=_split[0];
		if(_commands[_command]){
			let _options=false;
			if(_split[1]){
				_options=_split[1].split(":");
			}
			_commands[_command](_options, message)
				.then((msg)=>{
					_debug("Message sent.");
				})
				.catch((error)=>{
					_debug("Something went wrong. %o",error);
					message.reply(["Something went wrong.",error]);
				});
		}
	}
});

let _commands={
	"!add_goal":(_options, message)=>{
		let _debug=debug('!add_goal');
		_debug("Adding goal.\n_options: %o");
		_store.goals.push({name:_options[0],description:_options[1]});
		return _commands["!show_notes"](null, message);
	},
	"!add_item":(_options, message)=>{
		let _debug=debug('!add_item');
		_debug("Adding item.\n_options: %o");
		_store.items.push({name:_options[0],description:_options[1]});
		return _commands["!show_notes"](null, message);
	},
	"!add_location":(_options, message)=>{
		let _debug=debug('!add_location');
		_debug("Adding location.\n_options: %o");
		_store.locations.push({name:_options[0],description:_options[1]});
		return _commands["!show_notes"](null, message);
	},
	"!add_person":(_options, message)=>{
		let _debug=debug('!add_person');
		_debug("Adding person.\n_options: %o");
		_store.persons.push({name:_options[0],description:_options[1]});
		return _commands["!show_notes"](null, message);
	},
	"!clear":(_options, message)=>{
		let _debug=debug('!clear');
		_debug("Clearing out the store.");
		_store={
			locations:[],
			persons:[],
			items:[],
			goals:[]
		}
		_debug("Store cleared.");
		return _commands["!show_notes"](null, message);
	},
	"!complete_goal":(_options, message)=>{
		let _debug=debug('!complete_goal');
		_debug("Completing a goal. %o", _options);
		let _index=_options[0];
		if(_store.goals[_index]){
			_store.goals[_index].complete=true;
		}
		return _commands["!show_notes"](null, message);
	},
	"!help":(_options, message)=>{
		let _debug=debug('!help');
		_debug("Sending help.");
		return message.reply(`
Hello! I'm a bot to help you with your theater of mind game. I keep notes for you. Whenever I do any of these activities (except this one), I also show my notes afterwards, and pin them to the pinned messages board. I will only ever have one message pinned at a time.

!add_goal <name>:<description> #Add a goal necessary to complete the scene. Example: !add_goal Get honey:Pooh needs a jar of honey. He doesn't care how you get it, he just wants one.
!add_item <name>:<description> #Add an item relevant to the scene. Example !add_item Honey jar:A jar of honey. Pooh wanted one of these.
!add_person <name>:<description> #Add a person relevant to the scene. Example !add_person Pooh:A large intelligent bear. He wanted a jar of honey.
!add_location <name>:<description> #Add a location relevant to the scene. Example !add_location Pooh's house:A large hollowed tree stump that Pooh lives in.
!complete_goal <id> #When you complete a goal, be sure to run this to update the list.
!clear #Clear the goals, items, persons, and locations.
!help #Show this message!
		`);
	},
	"!show_notes":(_options, message)=>{
		let _goals="You have no goals.";
		let _people="You have met no people.";
		let _locations="You have not discovered any locations.";
		let _items="You have no found any items.";

		if(_store.goals.length > 0){
			_goals=_store.goals.reduce((acc,curr,id)=>{
				if(curr.complete){
					return `${acc}\n${id}. ~~${curr.name}: ${curr.description}~~`;
				}else{
					return `${acc}\n${id}. ${curr.name}: ${curr.description}`;
				}
			},"");
		}
		if(_store.persons.length > 0){
			_people=_store.persons.reduce((acc,curr,id)=>{
				return `${acc}\n${id}. ${curr.name}: ${curr.description}`;
			},"");
		}
		if(_store.locations.length > 0){
			_locations=_store.locations.reduce((acc,curr,id)=>{
				return `${acc}\n${id}. ${curr.name}: ${curr.description}`;
			},"");
		}
		if(_store.items.length > 0){
			_items=_store.items.reduce((acc,curr,id)=>{
				return `${acc}\n${id}. ${curr.name}: ${curr.description}`;
			},"");
		}

		return message.channel.send(
`**Goals**

${_goals}

**People**

${_people}

**Locations**

${_locations}

**Items**

${_items}`
		).then((_message)=>{
			if(_store.lastPin){
				return _message
					.channel
					.fetchMessage(_store.lastPin)
					.then((__message)=>__message.unpin())
					.then(()=>{
						_store.lastPin=_message.id;
						return Promise.resolve(_message);
					});
			}else{
				_store.lastPin=_message.id;
				return Promise.resolve(_message);
			}
		}).then((_message)=>{
			return _message.pin();
		});
	}
};


client.login("CHANGE_ME");
